// this script gets piped through cpp to sh
// cpp reads imexttypes.h and substitutes IMAGER_API_VERSION, sh echoes it out
cat >/dev/null <<END-OF-IMAGER
#include "../imexttypes.h"
END-OF-IMAGER

echo IMAGER_API_VERSION
