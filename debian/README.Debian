Dependencies for Imager
============================

This information is only relevant for maintainers of Debian packages
compiled against Imager.

These packages need to depend on the perl-imagerapi-* virtual package
provided by libimager-perl.
To generate this dependency please do one of the following:

- debhelper using dh:

Use the "perl_imager" addon and ensure ${perl:Depends} is used in debian/control.
This requires a build-dependency on libimager-perl (>= 0.95+dfsg).

The debian/rules file might look as follows:

  #!/usr/bin/make -f
  %:
          dh $@ --with perl_imager

Alternatively, since libimager-perl/1.007+dfsg-1 you can just build depend
on dh-sequence-perl-imager and omit the "--with perl_imager" part.

- old-style debhelper:

Run dh_perl_imager after dh_perl and ensure ${perl:Depends} is used in
debian/control.  This requires a build dependency on libimager-perl (>= 0.95+dfsg).

- packages not using debhelper:

Include /usr/share/libimager-perl/perl-imagerapi.make in debian rules and use
PERL_IMAGERAPI_DEPENDS.  This requires a build-dependency on
libimager-perl (>= 0.95+dfsg).

See https://bugs.debian.org/693003 for a discussion why this dependency is
required.
